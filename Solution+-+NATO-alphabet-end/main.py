from selenium import webdriver

chrome_driver_path = "/Users/andriwicaksono/Development/chromedriver"
driver = webdriver.Chrome(executable_path=chrome_driver_path)

#driver.get("https://www.amazon.com/DJI-Pocket-Stabilizer-Pocket-Sized-ActiveTrack/dp/B08J7FL57P/ref=sr_1_3")

# price = driver.find_element_by_id("priceblock_ourprice")
# print(price.text)

driver.get("https://python.org")
# search_bar = driver.find_element_by_name("q")
# print(search_bar.get_attribute("placeholder"))

# logo = driver.find_element_by_class_name("python-logo")
# print(logo.size)

# documentation_link = driver.find_element_by_css_selector(".documentation-widget a")
# print(documentation_link.text)

# XPath, copy xpath
# bug_link = driver.find_element_by_xpath('blablabla')
# print(bug_link.text)
event_times = driver.find_elements_by_css_selector(".event-widget time")
event_names = driver.find_elements_by_css_selector(".event-widget li a")
events = {}

for n in range(len(event_times)):
    events[n] = {
        "time": event_times[n].text,
        "name": event_names[n].text,
    }

print(events)

# driver.close() close tab
driver.quit()